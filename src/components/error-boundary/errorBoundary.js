import React, { Component } from 'react';
import './errorBoundary.css'
import ErrorIndicator from "../error-indicatior/errorIndicator";

class ErrorBoundary extends Component {
  state = {
    err: false
  };

  componentDidCatch(error, errorInfo) {
    this.setState({
      err: true
    })
  }

  render () {
    if (this.state.err) {
      return <ErrorIndicator />
    }
    return this.props.children
  }
}

export default ErrorBoundary;
