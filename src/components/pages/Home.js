import React from 'react';
import BookListContainer from "../book-list";

const Home = () => {

  return (
    <BookListContainer />
  )
};

export default Home;
