import React from 'react';
import './book-list-item.css';

const bookListItem = ({book, onAddedToCart}) => {
  const {title, author, image, price} = book;

  return (
    <div className='book-item'>
      <img src={image} alt='' className='book-img' />
      <div className='book-details'>
        <div>
          <h3 className='book-title'>{title}</h3>
          <span className='book-author'>{author}</span>
          <div>
            <b className='book-price'>${price}</b>
          </div>
        </div>
        <button onClick={onAddedToCart}
          className="btn btn-info add-to-card">Add to cart</button>
      </div>
    </div>
  )
};

export default bookListItem;
