import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { WithBookstoreService } from '../hok';
import { Home, Cart } from "../pages";
import Header from "../header";
import './App.css';
import ShoppingTable from "../shopping-table";

const App = ({ bookstoreService }) => {
  return (
    <div className='container'>
      <Header numItems={5} total={230} />
      <Switch>
        <Route path='/' component={ Home } exact />
        <Route path='/cart' component={ Cart } />
      </Switch>
      <ShoppingTable />
    </div>
  )
};

export default WithBookstoreService()(App);
