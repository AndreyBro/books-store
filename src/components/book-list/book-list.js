import React, {Component} from 'react';
import connect from 'react-redux/es/connect/connect';
import {WithBookstoreService} from '../hok'
import {fetchBooks, bookAddedToCart} from '../../store/actions/booksLoaded';
import Spinner from '../spinner/spinner';
import './book-list.css';
import ErrorIndicator from "../error-indicatior/errorIndicator";
import BooksList from "../../containers/booksList/booksList";

class BookListContainer extends Component {
  componentDidMount() {
    this.props.fetchBooks();
  }

  render () {
    const { books, loading, error, onAddedToCart } = this.props;

    if (loading) {
      return <Spinner />
    }
    if (error) {
      return <ErrorIndicator />
    }

    return <BooksList books={books} onAddedToCart={onAddedToCart}/>
  }
}

const mapStateToProps = ({ books, loading, error }) => {
  return { books, loading, error }
};

const mapDispatchToProps = (dispatch, { bookstoreService }) => {
  return {
    fetchBooks: fetchBooks(bookstoreService, dispatch),
    onAddedToCart: (id) => dispatch(bookAddedToCart(id))
  }
};

export default WithBookstoreService()(
  connect(mapStateToProps, mapDispatchToProps)(BookListContainer)
);
