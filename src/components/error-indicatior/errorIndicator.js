import React  from 'react';

const ErrorIndicator = () => {
  return <div className="errorBoundary">Something went wrong with component</div>
};

export default ErrorIndicator;
