import React from 'react';
import './header.css';
import {Link} from 'react-router-dom';

const Header = ({ numItems, total }) => {
  return (
    <header className='header'>
      <Link to='/' className='logo-text-dark'>ReStore</Link>
      <Link to='/cart' className='basket'>
        <i className='cart-icon fa fa-shopping-cart'>{numItems} items (${total})</i>
      </Link>
    </header>
  )
};

export default Header;
