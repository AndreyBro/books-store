const booksLoaded = (newBooks) => {
  return  {
    type: 'FETCH_BOOKS_SUCCESS',
    payload: newBooks
  }
};

const booksRequested = () => {
  return  {
    type: 'FETCH_BOOKS_REQUEST'
  }
};

const booksError = (error) => {
  return  {
    type: 'FETCH_BOOKS_FAILURE',
    payload: error
  }
};

const fetchBooks = (bookstoreService, dispatch) => () => {
  dispatch(booksRequested());
  bookstoreService.getBooks()
    .then((res) => dispatch(booksLoaded(res)))
    .catch((error) => dispatch(booksError(error)))
};

export const bookAddedToCart = (bookId) => {
  return {
    type: 'BOOK_ADDED_TO_CART',
    payload: bookId
  }
};

export {
  fetchBooks
}
