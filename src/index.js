import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './components/app/App.js';
import ErrorBoundary from './components/error-boundary/errorBoundary';
import BookstoreService from './services/bookstoreService';
import store from './store/store';
import { BookstoreServiceProvider } from './components/bookstore-servcice-context'

const bookStoreService = new BookstoreService();

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <BookstoreServiceProvider value={bookStoreService}>
        <Router>
          <App />
        </Router>
      </BookstoreServiceProvider>
    </ErrorBoundary>
  </Provider>,
  document.getElementById('root')
);
