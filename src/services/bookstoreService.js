class BookstoreService {
  data = [
    {
      id: 1,
      title: '12 rules for life',
      author: 'Jordan Peterson',
      price: '24.49',
      image: 'https://i1.rozetka.ua/goods/12841100/102172944_images_12841100526.jpg'
    },
    {
      id: 2,
      title: 'The subtle art of not giving a f*',
      author: 'Mark Manson',
      price: '23.99',
      image: 'https://i.ebayimg.com/00/s/NTAwWDMzMg==/z/b4kAAOSwAmJcLff-/$_57.JPG?set_id=8800005007'
    }
  ];

  getBooks() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.data)
      }, 1000)
    })
  }
}

export default BookstoreService;
